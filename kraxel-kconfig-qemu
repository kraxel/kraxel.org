#!/bin/sh

##############################################################

disable="

CONFIG_X86_EXTENDED_PLATFORM
CONFIG_X86_PLATFORM_DEVICES
CONFIG_XEN
CONFIG_PCCARD
CONFIG_PCMCIA
CONFIG_CARDBUS
CONFIG_IPX
CONFIG_ATALK
CONFIG_AX25
CONFIG_IRDA
CONFIG_BT
CONFIG_WIRELESS
CONFIG_FIREWIRE
CONFIG_WLAN
CONFIG_WAN
CONFIG_ISDN
CONFIG_INPUT_JOYSTICK
CONFIG_INPUT_TOUCHSCREEN
CONFIG_HWMON
CONFIG_MEDIA_SUPPORT
CONFIG_INFINIBAND
CONFIG_AGP
CONFIG_DRM
CONFIG_CPU_FREQ
CONFIG_ATM_DRIVERS
CONFIG_SERIAL_8250_EXTENDED

CONFIG_REISERFS_FS
CONFIG_OCFS2_FS
CONFIG_NCP_FS

CONFIG_SCSI
CONFIG_ATA
CONFIG_ETHERNET
CONFIG_FB
CONFIG_USB_SERIAL

"

##############################################################

enable="

# virtio
CONFIG_VIRTIO=y
CONFIG_VIRTIO_RING=y
CONFIG_VIRTIO_PCI=y
CONFIG_VIRTIO_BLK=y
CONFIG_VIRTIO_NET=y
CONFIG_VIRTIO_CONSOLE=y
CONFIG_VIRTIO_BALLOON=m
CONFIG_HW_RANDOM_VIRTIO=m

# scsi
CONFIG_SCSI=y
CONFIG_SCSI_PROC_FS=y
CONFIG_BLK_DEV_SD=y
CONFIG_CHR_DEV_ST=m
CONFIG_BLK_DEV_SR=y
CONFIG_CHR_DEV_SG=y
CONFIG_SCSI_MULTI_LUN=y
CONFIG_SCSI_CONSTANTS=y
CONFIG_SCSI_LOGGING=y
CONFIG_SCSI_SCAN_ASYNC=y
CONFIG_VMWARE_PVSCSI=m
CONFIG_MEGARAID_SAS=m
CONFIG_SCSI_SYM53C8XX_2=m
CONFIG_SCSI_VIRTIO=y
CONFIG_USB_STORAGE=m
CONFIG_USB_UAS=m

# ide/ata
CONFIG_ATA=y
CONFIG_SATA_AHCI=y
CONFIG_ATA_PIIX=y

# network
CONFIG_ETHERNET=y
CONFIG_E1000=m
CONFIG_E1000E=m
CONFIG_8139CP=m
CONFIG_8139TOO=m
CONFIG_VMXNET3=m

# video
CONFIG_FB=y
CONFIG_FB_VESA=y
CONFIG_FB_EFI=y
CONFIG_FB_VGA16=m
CONFIG_FB_CIRRUS=m

# sound
CONFIG_SOUND=m
CONFIG_SND=m
CONFIG_SND_INTEL8X0=m
CONFIG_SND_INTEL8X0M=m
CONFIG_SND_HDA_INTEL=m
CONFIG_SND_HDA_GENERIC=y

# usb hcd
CONFIG_USB_XHCI_HCD=y
CONFIG_USB_EHCI_HCD=y
CONFIG_USB_OHCI_HCD=y
CONFIG_USB_UHCI_HCD=y

# usb devs
CONFIG_USB_HID=y
CONFIG_USB_SERIAL=y
CONFIG_USB_SERIAL_GENERIC=y
CONFIG_SND_USB=y
CONFIG_SND_USB_AUDIO=m

"

##############################################################

function msg() {
	 txt="$1"
	 echo ""
	 echo "###"
	 echo "### $txt"
	 echo "###"
}

##############################################################

msg "sanize"
yes "" | make oldconfig > /dev/null 2>&1

msg "disable stuff"
for item in $disable; do
	sed -i -e "/${item}[ =]/d" .config
done
for item in $disable; do
	echo "# ${item} is not set" >> .config
done
make oldconfig

msg "check: disable stuff"
count=0
for item in $disable; do
	if grep "${item}=" .config; then
		count=$(( $count + 1 ))
	fi
done
if test "$count" = "0"; then
	echo "### OK"
else
	echo "### $count mismatch(es)"
fi

msg "enable stuff"
echo "$enable" >> .config
yes "" | make oldconfig > /dev/null 2>&1

msg "check: enable stuff"
count=0
for wanted in $enable; do
	case "$wanted" in
	CONFIG_*)
		opt=${wanted%=*}
		have=$(grep "$opt[ =]" .config)
		if test "$wanted" != "$have"; then
			count=$(( $count + 1 ))
			echo "$opt -- ${have} -- want ${wanted#$opt=}"
		fi
		;;
	esac
done
if test "$count" = "0"; then
	echo "### OK"
else
	echo "### $count mismatch(es)"
fi
