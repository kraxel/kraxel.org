#!/bin/sh

################################################################################
# source stuff

# use git completion & prompt
for file in /usr/share/doc/git-*/contrib/completion/git-completion.bash \
            /usr/share/doc/git-*/contrib/completion/git-prompt.sh \
            /usr/share/git-*/contrib/completion/git-prompt.sh \
; do
	test -f "$file" || continue
	source "$file"
done

################################################################################
# compose prompt

# base
PS1='\u@\h \w'

# git 
if set | grep -q __git_ps1; then
	PS1="${PS1}\$(__git_ps1 \" (%s)\")"
	export GIT_PS1_SHOWDIRTYSTATE="yes"
	export GIT_PS1_SHOWCOLORHINTS="yes"
fi

# terminals
case "$TERM" in
screen)
	if test "$WINDOW" != ""; then
		PS1="w$WINDOW ${PS1}"
	fi
	;;
esac

# finalize
PS1="\[\033[0m\]${PS1}# "
