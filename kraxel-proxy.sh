#!/bin/sh

# caching squid
export http_proxy="http://proxy.home.kraxel.org:3128"
export https_proxy="http://proxy.home.kraxel.org:3128"
export no_proxy="localhost"

# minikube & minishift
for net in \
	.home.kraxel.org \
	.redhat.com	\
	\
	.nip.io		\
	.cluster.local	\
	.svc		\
	\
	127.0.0.1	\
	192.168.86.1	\
	\
	192.168.0.0/16	\
	172.30.0.0/16	\
; do
	no_proxy="${no_proxy},${net}"
done
