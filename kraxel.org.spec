Name:         kraxel.org
License:      none
Group:        kraxel
Version:      527
Release:      1%{?dist}.kraxel
Summary:      kraxel.org home network stuff

Source0:      %{name}.fedora.repo
Source4:      %{name}.rhelx.repo

Source5:      auto.master.%{name}
Source6:      auto.map.%{name}
Source7:      kraxel-serial.sh
Source8:      kraxel-proxy.sh
Source9:      kraxel-prompt.sh
Source10:     net-kraxel.conf
Source11:     RPM-GPG-KEY-kraxel.org
Source18:     kraxel.org.modules.conf

Source40:     qemu-gfx.conf
Source41:     qemu-build-deps
Source45:     qxl-mode
Source46:     qxl-testmodes
Source47:     qxl-info

Source50:     rh-repos
Source51:     gai-redhat.conf
Source52:     rh-vpn

Source61:     redhat.com.krb5
Source62:     kraxel.org.collectd

Source70:     vpn-rh-ams
Source71:     vpn-rh-phx
Source79:     kraxel-nm.conf

Source80:     kraxel-check-kernel
Source81:     kraxel-post-install
Source82:     kraxel-s3
Source83:     kraxel-check-idle
Source84:     kraxel-bench-usbstick
Source85:     kraxel-stress-usbstick
Source86:     kraxel-kconfig-qemu
Source87:     kraxel-setup-guest
Source88:     kraxel-format-usbstick
Source89:     kraxel-big-rpms

Source93:     kraxel-diskspeed
Source94:     kraxel-ssh-keys

Source95:     kraxel-backup
Source97:     youtube-dl-wrapper
Source99:     fedora-repos

Source200:    mini-ca.kraxel.org.crt
Source201:    easyrsa.kraxel.org.crt
Source202:    step-ca.kraxel.org.crt
Source210:    RedHatISCA-2009.crt
Source211:    RedHatISCA-2015.crt
Source212:    RedHat-ipa.crt
Source213:    RedHatISCA-2022.crt

Buildroot:    %{_tmppath}/root-%{name}-%{version}
BuildArch:    noarch


%package krb5
Summary:      kraxel.org krb5 support
Requires:     krb5-workstation
Requires:     sssd-krb5
#Requires:     pam_krb5

%description krb5
kraxel.org krb5 support


%package tools
Summary:      kraxel.org common tools
Requires:     at cronie-anacron
#Requires:     mailx
Requires:     tmux
Requires:     which
Requires:     perl
Requires:     bind-utils
Requires:     pciutils usbutils
Requires:     jq
%if "%{?fedora}" != ""
Requires:     xterm-resize
%else
Requires:     xterm
%endif

%description tools
kraxel.org common tools


%package collectd
Summary:      kraxel.org collectd config
Requires:     collectd
#Requires:     collectd-virt
#Requires:     collectd-sensors

%description collectd
kraxel.org collectd config


%package vpn
Summary:      kraxel.org vpn config

%description vpn
kraxel.org vpn config


%description
kraxel.org home network stuff
 - mirror repos.
 - homebrew packages.
 - mock configs.
 - autofs configs.
 - unbound cfg.
 - pkg deps

%prep

%build
echo "fedora:%{?fedora}:"
echo "rhel:%{?rhel}:"

%install
mkdir -p %{buildroot}/etc/yum.repos.d
mkdir -p %{buildroot}/etc/profile.d
mkdir -p %{buildroot}/etc/sysctl.d
mkdir -p %{buildroot}/etc/mock
mkdir -p %{buildroot}/etc/pki/rpm-gpg
mkdir -p %{buildroot}/etc/pki/tls/certs
mkdir -p %{buildroot}/etc/pki/ca-trust/source/anchors
mkdir -p %{buildroot}/bin
mkdir -p %{buildroot}/root/config
mkdir -p %{buildroot}/etc/unbound/local.d
mkdir -p %{buildroot}/etc/modprobe.d
mkdir -p %{buildroot}/etc/krb5.conf.d
mkdir -p %{buildroot}/etc/collectd.d
mkdir -p %{buildroot}/etc/NetworkManager/system-connections
mkdir -p %{buildroot}/etc/NetworkManager/conf.d

%if "%{?fedora}" != ""
cp %{SOURCE0} %{buildroot}/etc/yum.repos.d
%endif
%if "%{?rhel}" == "8"
cp %{SOURCE4} %{buildroot}/etc/yum.repos.d
%endif
%if "%{?rhel}" == "9"
cp %{SOURCE4} %{buildroot}/etc/yum.repos.d
%endif

cp %{SOURCE5} %{buildroot}/etc
cp %{SOURCE6} %{buildroot}/etc
cp %{SOURCE7} %{buildroot}/etc/profile.d
cp %{SOURCE8} %{buildroot}/etc/profile.d
cp %{SOURCE9} %{buildroot}/etc/profile.d
cp %{SOURCE10} %{buildroot}/etc/sysctl.d
cp %{SOURCE11} %{buildroot}/etc/pki/rpm-gpg
cp %{SOURCE18} %{buildroot}/etc/modprobe.d/kraxel.org.conf

cp %{SOURCE200} %{buildroot}/etc/pki/tls/certs
cp %{SOURCE201} %{buildroot}/etc/pki/tls/certs
cp %{SOURCE202} %{buildroot}/etc/pki/tls/certs
cp %{SOURCE210} %{buildroot}/etc/pki/tls/certs
cp %{SOURCE211} %{buildroot}/etc/pki/tls/certs
cp %{SOURCE212} %{buildroot}/etc/pki/tls/certs
cp %{SOURCE213} %{buildroot}/etc/pki/tls/certs
for src in \
	%{SOURCE200} \
	%{SOURCE201} \
	%{SOURCE202} \
	%{SOURCE210} \
	%{SOURCE211} \
	%{SOURCE212} \
	%{SOURCE213} \
; do
	file=$(basename $src)
	ln -s -v ../../../tls/certs/$file \
		%{buildroot}/etc/pki/ca-trust/source/anchors/$file
done

cp %{SOURCE40} %{SOURCE41} %{buildroot}/root/config

cp %{SOURCE45}             %{buildroot}/bin
cp %{SOURCE46}             %{buildroot}/bin
cp %{SOURCE47}             %{buildroot}/bin

cp %{SOURCE50}             %{buildroot}/bin
cp %{SOURCE51}             %{buildroot}/etc
cp %{SOURCE52}             %{buildroot}/bin

cp %{SOURCE61} %{buildroot}/etc/krb5.conf.d/redhat_com
cp %{SOURCE62} %{buildroot}/etc/collectd.d/kraxel.conf

cp %{SOURCE70} %{buildroot}/etc/NetworkManager/system-connections/
cp %{SOURCE71} %{buildroot}/etc/NetworkManager/system-connections/
chmod 600      %{buildroot}/etc/NetworkManager/system-connections/*
cp %{SOURCE79} %{buildroot}/etc/NetworkManager/conf.d/

cp %{SOURCE80} %{SOURCE81} %{buildroot}/bin
cp %{SOURCE83} %{SOURCE84} %{buildroot}/bin
cp %{SOURCE85} %{SOURCE86} %{buildroot}/bin
cp %{SOURCE87} %{SOURCE88} %{buildroot}/bin
cp %{SOURCE89}             %{buildroot}/bin

cp %{SOURCE93}             %{buildroot}/bin
cp %{SOURCE94}             %{buildroot}/bin
cp %{SOURCE95}             %{buildroot}/bin

cp %{SOURCE97}             %{buildroot}/bin
ln -s youtube-dl-wrapper   %{buildroot}/bin/youtube-dl-default
ln -s youtube-dl-wrapper   %{buildroot}/bin/youtube-dl-fast
ln -s youtube-dl-wrapper   %{buildroot}/bin/youtube-dl-sd
ln -s youtube-dl-wrapper   %{buildroot}/bin/youtube-dl-hd
ln -s youtube-dl-wrapper   %{buildroot}/bin/youtube-dl-fullhd
cp %{SOURCE99}             %{buildroot}/bin

cp %{SOURCE82} %{buildroot}/bin/kraxel-s3
cp %{SOURCE82} %{buildroot}/bin/kraxel-s4

%post -p /bin/kraxel-post-install

%post collectd
systemctl enable --now collectd

%files
%defattr(-,root,root,-)
/etc/yum.repos.d/kraxel.org.*.repo
/etc/profile.d/kraxel-*.sh
/etc/sysctl.d/net-kraxel.conf
/etc/pki/rpm-gpg/RPM-GPG-KEY-kraxel.org
/etc/pki/tls/certs/*.kraxel.org.crt
/etc/pki/tls/certs/RedHat*.crt
/etc/pki/ca-trust/source/anchors/*.kraxel.org.crt
/etc/pki/ca-trust/source/anchors/RedHat*.crt
/etc/auto.*.kraxel.org
/etc/modprobe.d/kraxel.org.conf
/etc/gai-*.conf
/bin/kraxel-*
/bin/qxl-*
/bin/rh-repos
/bin/youtube-dl-*
/root/config

%files krb5
/etc/krb5.conf.d/redhat_com

%files collectd
/etc/collectd.d/kraxel.conf

%files vpn
/bin/rh-vpn
/etc/NetworkManager/conf.d/*
/etc/NetworkManager/system-connections/*

%files tools
/bin/fedora-repos
