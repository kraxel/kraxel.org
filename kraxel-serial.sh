#!/bin/sh

case "$(tty)" in
/dev/ttyS0 | /dev/ttyAMA0 | /dev/hvc0 | /dev/ttysclp0)
	TERM=xterm
	if test -x "$(which resize 2>/dev/null)"; then
		eval $(resize)
	fi
	;;
esac
